# asciidoctools

Docker image with [AsciiDoc](http://asciidoc.org/) tools.

## Usage

### Generating HTML from ADOC

```
docker run
  --rm \
  --mount type=bind,source="/path/to/your/docs/folder",target=/docs \
  robertbus/asciidoctools \
  asciidoctor -a stylesheet=/docs/foo.css /docs/bar.adoc
```

### Generating PDF from HTML

```
docker run
  --rm \
  --mount type=bind,source="/path/to/your/docs/folder",target=/docs \
  robertbus/asciidoctools \
  wkhtmltopdf /docs/bar.html /docs/baz.pdf
```

## Useful links

* [asciidoctor](https://asciidoctor.org/)
* [wkhtmltopdf](https://wkhtmltopdf.org/)
