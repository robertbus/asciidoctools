FROM debian:stretch-slim

# Debian comes with a buggy version of wkhtmltopdf (0.12.3.2), which prevents it
# from running it properly in a container without an X screen. For details see
# https://github.com/wkhtmltopdf/wkhtmltopdf/issues/2037

# We'll get and install the latest official fixed version manually instead of
# using the version from the Debian repo.

ADD https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb /tmp

RUN apt-get update && \
    apt-get upgrade && \
    apt-get install --no-install-recommends -y \
    asciidoc \
    asciidoctor \
    pandoc && \
    dpkg -i /tmp/wkhtmltox_0.12.5-1.stretch_amd64.deb || true && \
    apt-get install --no-install-recommends -y -f && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/wkhtmltox_0.12.5-1.stretch_amd64.deb
